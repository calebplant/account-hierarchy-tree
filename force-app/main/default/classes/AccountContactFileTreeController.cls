public with sharing class AccountContactFileTreeController {
    
    /*
    * Returns 5 most recently created Accounts along with their associated Contacts, the the
    * Contact's associated Files
    */
    @AuraEnabled(cacheable=true)
    public static List<AccountHierarchy> getMostRecentAccountsWithAssociatedFiles()
    {
        List<Account> recentAccs = [SELECT Id, Name, (SELECT Id, Name FROM Contacts)
                                    FROM Account
                                    ORDER BY createdDate DESC LIMIT 5];
        // System.debug('Recent Accounts: ' + recentAccs);
  
        Map<Id, List<ContentDocumentLink>> docLinkByLinkedEntityId = getAssociatedFiles(recentAccs);


        // List<AccountHierarchy> result =  wrapAccountHierarchy(recentAccs, docLinkByLinkedEntityId);
        // for(AccountHierarchy eachAccHier : result) {
        //     System.debug(eachAccHier);
        // }
        // return result;

        return wrapAccountHierarchy(recentAccs, docLinkByLinkedEntityId);
    }

    /*
    * Returns a map of a list of ContentDocumentLinks. The key (a Contact Id) returns a list of ContentDocumentLinks
    * associated with that Contact
    */
    private static Map<Id, List<ContentDocumentLink>> getAssociatedFiles(List<Account> accs)
    {
        // Map Contacts associated with the passed Accounts by their Id
        List<Contact> retrievedContacts = new List<Contact>();
        for(Account eachAcc : accs) {
            retrievedContacts.addAll(eachAcc.Contacts);
        }
        Map<Id, Contact> contactById = new Map<Id, Contact>(retrievedContacts);
        // System.debug('Contacts: ' + retrievedContacts);

        if(contactById.isEmpty()) {
            return new Map<Id, List<ContentDocumentLink>>(); // no contacts associated with the accounts
        }

        // Get ContentDocumentLinks associated with retrieved Contacts
        List<ContentDocumentLink> docLinks = [SELECT ContentDocumentId, LinkedEntityId,
                                                     ContentDocument.Title, ContentDocument.FileExtension
                                            FROM ContentDocumentLink
                                            WHERE LinkedEntityId IN :contactById.keySet()];

        // Build result
        Map<Id, List<ContentDocumentLink>> docLinkByLinkedEntityId = new Map<Id, List<ContentDocumentLink>>();
        for(ContentDocumentLink eachDocLink : docLinks) {
            if(docLinkByLinkedEntityId.containsKey(eachDocLink.LinkedEntityId)) {
                List<ContentDocumentLink> updatedLinks = docLinkByLinkedEntityId.get(eachDocLink.LinkedEntityId);
                updatedLinks.add(eachDocLink);
                docLinkByLinkedEntityId.put(eachDocLink.LinkedEntityId, updatedLinks);
            } else {
                docLinkByLinkedEntityId.put(eachDocLink.LinkedEntityId, new List<ContentDocumentLink>{eachDocLink});
            }
        }
        // System.debug(docLinkByLinkedEntityId);
        return docLinkByLinkedEntityId;
    }

    /*
    * Wraps up the results in a hierarchy to be used by the lightning component.
    * This way we get a nice JSON response (Account -> Contacts -> Files)
    */
    private static List<AccountHierarchy> wrapAccountHierarchy(List<Account> accs, Map<Id, List<ContentDocumentLink>> docLinkByLinkedEntityId)
    {
        List<AccountHierarchy> wrappedResult = new List<AccountHierarchy>();

        for(Account eachAcc : accs) {
            AccountHierarchy newAcc = new AccountHierarchy(eachAcc.Id, eachAcc.Name);

            for(Contact eachCon : eachAcc.Contacts) {
                ContactHierarchy newCon = new ContactHierarchy(eachCon.Id, eachCon.Name);

                // Add files if Contact has any
                if(docLinkByLinkedEntityId.containsKey(eachCon.Id)) {
                    newCon.attachLinks(docLinkByLinkedEntityId.get(eachCon.Id));
                    // Update number of files for Contact and Account
                    newCon.NumOfFiles = NewCon.Files.size();
                    newAcc.NumOfFiles += newCon.NumOfFiles;
                }

                newAcc.addContact(newCon);
            }
            wrappedResult.add(newAcc);
        }

        return wrappedResult;
    }
}
