public with sharing class AccountHierarchy {
    @AuraEnabled    
    public Id Id;
    @AuraEnabled
    public String Name;
    @AuraEnabled
    public List<ContactHierarchy> Contacts = new List<ContactHierarchy>();
    @AuraEnabled
    public Integer NumOfFiles;

    public AccountHierarchy(Id id, String name) {
        this.Id = id;
        this.Name = name;
        this.NumOfFiles = 0;
    }

    public void addContact(ContactHierarchy con) {
        this.Contacts.add(con);
    }
}
