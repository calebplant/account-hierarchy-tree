public class ContactHierarchy {
    @AuraEnabled
    public Id Id;
    @AuraEnabled
    public String Name;
    @AuraEnabled
    public List<FlattenedDocument> Files = new List<FlattenedDocument>();
    @AuraEnabled
    public Integer NumOfFiles {get; set;}

    public ContactHierarchy(Id id, String name) {
        this.Id = id;
        this.Name = name;
        this.NumOfFiles = 0;
    }

    public void attachLinks(List<ContentDocumentLink> docLinks) {
        List<FlattenedDocument> flattenedDocs = new List<FlattenedDocument>();
        for(ContentDocumentLink eachDocLink : docLinks) {
            flattenedDocs.add(new FlattenedDocument(eachDocLink));
            
        }
        this.Files.addAll(flattenedDocs);
    }
}
