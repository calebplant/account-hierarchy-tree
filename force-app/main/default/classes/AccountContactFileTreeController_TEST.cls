@isTest
public with sharing class AccountContactFileTreeController_TEST {

    private static string TEST_ACC_1_NAME = 'testAcc1';
    private static string TEST_ACC_2_NAME = 'testAcc2';
    private static Integer NUM_CVS_CON_1 = 5;
    private static Integer NUM_CVS_CON_2 = 10;
    private static Integer NUM_RECENT_ACC_RETURNED = 5;
    
    @TestSetup
    static void makeData(){
        // Accounts
        List<Account> accs = new List<Account>();
        for(Integer i=0; i < 10; i++) {
            accs.add(new Account(Name='loopAcc'+i));
        }
        insert accs;
        // Set date created to yesterday
        Datetime yesterday = Datetime.now().addDays(-1);
        for(Account eachAcc : accs) {
            Test.setCreatedDate(eachAcc.Id, yesterday);
        }

        List<Account> recentAccounts = new List<Account>();
        recentAccounts.add(new Account(Name=TEST_ACC_1_NAME));
        recentAccounts.add(new Account(Name=TEST_ACC_2_NAME));
        insert recentAccounts;

        // Contacts associated w/ recent account
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(LastName='Contact1_acc1', AccountId=recentAccounts[0].Id));
        cons.add(new Contact(LastName='Contact2_acc1', AccountId=recentAccounts[0].Id));
        cons.add(new Contact(LastName='Contact3_acc2', AccountId=recentAccounts[1].Id));
        insert cons;

        // Files
        List<ContentVersion> con1Cvs = new List<ContentVersion>();
        for(Integer i=0; i < NUM_CVS_CON_1; i++) {
            con1Cvs.add(new ContentVersion(Title='CV1_'+i, PathOnClient='CV1' + i + '.jpg',
                                            VersionData= Blob.valueOf('cvContent1_'+i)));
        }
        List<ContentVersion> con2Cvs = new List<ContentVersion>();
        for(Integer i=0; i < NUM_CVS_CON_2; i++) {
            con2Cvs.add(new ContentVersion(Title='CV2_'+i, PathOnClient='CV2' + i + '.jpg',
                                            VersionData= Blob.valueOf('cvContent2_'+i)));
        }
        insert con1Cvs;
        insert con2Cvs;
        List<ContentDocument> con1Docs = [SELECT Id FROM ContentDocument WHERE Title LIKE 'CV1_%'];
        List<ContentDocument> con2Docs = [SELECT Id FROM ContentDocument WHERE Title LIKE 'CV2_%'];

        // Relate files to contacts
        List<ContentDocumentLink> con1DocLinks = new List<ContentDocumentLink>();
        for(ContentDocument eachDoc : con1Docs) {
            con1DocLinks.add(new ContentDocumentLink(ContentDocumentId=eachDoc.Id,
                                                     LinkedEntityId=cons[0].Id,
                                                     ShareType = 'V'));
        }
        List<ContentDocumentLink> con2DocLinks = new List<ContentDocumentLink>();
        for(ContentDocument eachDoc : con2Docs) {
            con2DocLinks.add(new ContentDocumentLink(ContentDocumentId=eachDoc.Id,
                                                     LinkedEntityId=cons[1].Id,
                                                     ShareType = 'V'));
        }
        insert con1DocLinks;
        insert con2DocLinks;
    }

    @isTest
    static void doesGetMostRecentAccountsWithAssociatedFilesFetchProperAccounts()
    {
        Test.startTest();
        List<AccountHierarchy> actual = AccountContactFileTreeController.getMostRecentAccountsWithAssociatedFiles();
        Test.stopTest();

        // Returned 5?
        System.assertEquals(NUM_RECENT_ACC_RETURNED, actual.size());
        // Check most recent 2 accounts were returned first
        System.assertEquals(TEST_ACC_1_NAME, actual[0].Name);
        System.assertEquals(TEST_ACC_2_NAME, actual[1].Name);
    }

    @isTest
    static void doesGetMostRecentAccountsWithAssociatedFilesFetchProperFiles()
    {
        Test.startTest();
        List<AccountHierarchy> actual = AccountContactFileTreeController.getMostRecentAccountsWithAssociatedFiles();
        Test.stopTest();

        // Get total number of files from both contacts with files
        Integer totalNumFiles = 0;
        for(AccountHierarchy eachAcc : actual) {
            if(!eachAcc.Contacts.isEmpty()) {
                for(ContactHierarchy eachCon : eachAcc.Contacts) {
                    totalNumFiles += eachCon.NumOfFiles;
                }
            }
        }
        System.assertEquals(NUM_CVS_CON_1 + NUM_CVS_CON_2, totalNumFiles);
    }
}

