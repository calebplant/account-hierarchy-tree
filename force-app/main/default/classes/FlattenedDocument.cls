public with sharing class FlattenedDocument {
    @AuraEnabled
    public Id Id;
    @AuraEnabled
    public Id LinkedEntityId;
    @AuraEnabled
    public Id ContentDocumentId;
    @AuraEnabled
    public String Title;
    @AuraEnabled
    public String FileExtension;


    public FlattenedDocument(ContentDocumentLink docLink) {
        this.Id = docLink.Id;
        this.LinkedEntityId = docLink.Id;
        this.ContentDocumentId = docLink.ContentDocumentId;
        this.Title = docLink.ContentDocument.Title;
        this.FileExtension = docLink.ContentDocument.FileExtension;
    }
}
