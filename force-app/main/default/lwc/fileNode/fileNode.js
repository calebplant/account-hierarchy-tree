import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import { NavigationMixin } from 'lightning/navigation';
import { deleteRecord } from 'lightning/uiRecordApi';

const ACTIONS = [
    {label: 'Download', name: 'download'},
    {label: 'Delete', name: 'delete'},
];

const COLUMNS = [
    // {label: 'ContentDocumentId', fieldName: 'ContentDocumentId'},
    {label: 'Title', fieldName: 'Title'},
    {label: 'Extension', fieldName: 'FileExtension'},
    {type: 'action', typeAttributes: {rowActions: ACTIONS, menuAlignment: 'right'}},
];

export default class FileNode extends NavigationMixin(LightningElement) {
    // Properties
    @api fileData;
    @api contactId;
    columns = COLUMNS;

    get hasFiles() {
        return this.fileData.length > 0 ? true : false;
    }

    // Event Handlers
    handleUploadFinished(event)
    {
        console.log('event details: ');
        console.log(JSON.parse(JSON.stringify(event)));
        
        const successEvent = new ShowToastEvent({
            title: 'Success!',
            message: 'Uploaded ' + event.detail.files[0].name,
            variant: 'success'
        });
        this.dispatchEvent(successEvent);

        this.dispatchEvent(new CustomEvent('upload'));
    }
    
    handleRowAction(event)
    {
        const action = event.detail.action;
        const row = event.detail.row;
        switch(action.name) {
            case 'download':
                console.log('Downloading file...');
                this.navigateToFile(row.ContentDocumentId);
                break;
            case 'delete':
                console.log('Deleting file...');
                this.deleteFile(row.ContentDocumentId);
                break;
        }
    }

    navigateToFile(documentId)
    {
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state : {
                recordIds: documentId,
                selectedRecordId:documentId
            }
          });
    }

    deleteFile(documentId)
    {
        deleteRecord(documentId)
        .then(() => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Record deleted',
                    variant: 'success'
                })
            );
            this.dispatchEvent(new CustomEvent('upload'));
        }).catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error deleting record',
                    message: error.body.message,
                    variant: 'error'
                })
            );
        });
    }

    // Utilities
    get jsonString() {
        return JSON.stringify(this.fileData);
    }
}