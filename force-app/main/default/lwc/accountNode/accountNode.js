import { LightningElement, api, wire } from 'lwc';
import SELECTMC from '@salesforce/messageChannel/NodeSelectionChannel__c';
import { MessageContext, subscribe, publish, APPLICATION_SCOPE } from 'lightning/messageService';

export default class AccountNode extends LightningElement {

    // Properties
    @api accountData;
    isExpanded = 'false';
    isSelected = 'false';
    
    get hasContacts() {
        return this.accountData.Contacts.length > 0 ? true : false;
    }

    get numOfFiles() {
        return this.accountData.NumOfFiles > 0 ? `(${this.accountData.NumOfFiles})` : '';
    }

    connectedCallback() {
        this.subscribeMC();
    }

    // Subscribe to Selection message channel
    @wire(MessageContext)
    messageContext;
    subscription = null;
    receivedMessage;

    subscribeMC() {
        if(this.subscription) {
            return;
        }
        this.subscription = subscribe(
            this.messageContext,
            SELECTMC,
            (message) => {
                this.handleMessage(message)
            },
            { scope: APPLICATION_SCOPE}
        );
    }

    unsubscribeMC() {
        unsubscribe(this.subscription);
        this.subscription = null;
    }

    // Event Handlers
    handleToggleExpand()
    {
        // console.log('accountNode :: handleExpandClick');
        this.toggleExpanded();
    }

    handleNameClick()
    {
        this.isSelected = 'true';
        this.toggleExpanded();
        console.log('accountNode :: handleNameClick');
        // Publish selection message
        const message = {nodeId: this.accountData.Id};
        publish(this.messageContext, SELECTMC, message);
    }

    handleFileUploaded()
    {
        this.dispatchEvent(new CustomEvent('upload'));
    }

    handleMessage(message) {
        // console.log('accountNode received message! ... ' + this.accountData.Id);
        this.isSelected = message.nodeId === this.accountData.Id ? true : false;
    }

    // Utilities
    toggleExpanded()
    {
        this.isExpanded = this.isExpanded === 'true' ? 'false' : 'true';
        // console.log('IsExpanded: ' + this.isExpanded);
    }
}