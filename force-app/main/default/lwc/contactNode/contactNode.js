import { LightningElement, api, wire } from 'lwc';
import SELECTMC from '@salesforce/messageChannel/NodeSelectionChannel__c';
import { MessageContext, subscribe, publish, APPLICATION_SCOPE } from 'lightning/messageService';

export default class ContactNode extends LightningElement {
    // Properties
    @api contactData;
    isExpanded = 'false';
    isSelected = 'false';

    get hasFiles() {
        return this.contactData.Files.length > 0 ? true : false;
    }

    get numOfFiles()
    {
        return this.contactData.NumOfFiles > 0 ? `(${this.contactData.NumOfFiles})` : '';
    }

    connectedCallback() {
        this.subscribeMC();
    }

    // Subscribe to Seleciton message channel
    @wire(MessageContext)
    messageContext;
    subscription = null;
    receivedMessage;

    subscribeMC() {
        if(this.subscription) {
            return;
        }
        this.subscription = subscribe(
            this.messageContext,
            SELECTMC,
            (message) => {
                this.handleMessage(message)
            },
            { scope: APPLICATION_SCOPE}
        );
    }

    unsubscribeMC() {
        unsubscribe(this.subscription);
        this.subscription = null;
    }

    // Event handlers
    handleToggleExpand()
    {
        console.log('accountNode :: handleExpandClick');
        this.toggleExpanded();
    }

    handleNameClick()
    {
        this.isSelected = 'true';
        this.toggleExpanded();
        console.log('contactNode :: handleNameClick');
        // Publish selection message
        const message = {nodeId: this.contactData.Id};
        publish(this.messageContext, SELECTMC, message);
    }

    handleFileUploaded()
    {
        this.dispatchEvent(new CustomEvent('upload'));
    }

    handleMessage(message) {
        // console.log('accountNode received message! ... ' + this.accountData.Id);
        this.isSelected = message.nodeId === this.contactData.Id ? true : false;
    }

    // Utilities
    toggleExpanded()
    {
        this.isExpanded = this.isExpanded === 'true' ? 'false' : 'true';
        console.log('IsExpanded: ' + this.isExpanded);
    }
}