import { LightningElement, wire } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import getMostRecentAccountsWithAssociatedFiles from '@salesforce/apex/AccountContactFileTreeController.getMostRecentAccountsWithAssociatedFiles';

export default class AccountContactFileTree extends LightningElement {
    
    accounts;
    error;

    _recordResponse;

    @wire(getMostRecentAccountsWithAssociatedFiles)
    getAccounts(response)
    {
        this._recordResponse = response;
        if(response.data) {
            console.log('response:');
            console.log(response.data);
            this.accounts = response.data;
        }
        if(response.error) {
            console.log('error:');
            console.log(response.error);
            this.error = response.data;
        }
    }

    handleFileUploaded()
    {
        console.log('AccountContractFileTree :: handleFileUploaded');
        refreshApex(this._recordResponse);
    }
}