# Account Hierarchy Component

## Demo

![Demo](media/demo.gif)

## Quick Links

### Components

#### accountContractFileTree

* [markup](force-app/main/default/lwc/accountContactFileTree/accountContactFileTree.html)
* [javascript](force-app/main/default/lwc/accountContactFileTree/accountContactFileTree.js)
* [css](force-app/main/default/lwc/accountContactFileTree/accountContactFileTree.css)

#### accountNode

* [markup](force-app/main/default/lwc/accountNode/accountNode.html)
* [javascript](force-app/main/default/lwc/accountNode/accountNode.js)

#### contactNode

* [markup](force-app/main/default/lwc/contactNode/contactNode.html)
* [javascript](force-app/main/default/lwc/contactNode/contactNode.js)

#### fileNode

* [markup](force-app/main/default/lwc/fileNode/fileNode.html)
* [javascript](force-app/main/default/lwc/fileNode/fileNode.js)

### Controllers

* [AccountContactFileTreeController](force-app/main/default/classes/AccountContactFileTreeController.cls)

### Wrapper Classes

* [AccountHierarchy](force-app/main/default/classes/AccountHierarchy.cls)
* [ContactHierarchy](force-app/main/default/classes/ContactHierarchy.cls)
* [FlattenedDocument](force-app/main/default/classes/FlattenedDocument.cls)


### Message Channel

* [NodeSelectionChannel](force-app/main/default/messageChannels/NodeSelectionChannel.messageChannel-meta.xml)

### Tests

* [AccountContactFileTreeController_TEST](force-app/main/default/classes/AccountContactFileTreeController_TEST.cls)



